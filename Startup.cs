using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using PedidosAPI.Data;

namespace Pedidos
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddDbContext<DataContext>(opt => opt.UseInMemoryDatabase("Database"));

            services.AddDbContext<DataContext>(opt => opt.UseNpgsql(Configuration.GetConnectionString("DefaultConnection")));


            services.AddScoped<DataContext, DataContext>();
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });


            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<DataContext>();
                AdicionarDadosTeste(context);
                // Seed the database.
            }


            //var context = app.ApplicationServices.GetService<DataContext>();
            
            
            
        }

        private void AdicionarDadosTeste(DataContext context)
        {
            //Adicionar clientes
            //PedidosAPI.Models.Cliente cliente1 = new PedidosAPI.Models.Cliente { id = 1, codigo = "11", nome = "Cliente 1" };
            //context.Cliente.Add(cliente1);

            //PedidosAPI.Models.Cliente cliente2 = new PedidosAPI.Models.Cliente { id = 2, codigo = "22", nome = "Cliente 2" };
            //context.Cliente.Add(cliente2);

            //PedidosAPI.Models.Cliente cliente3 = new PedidosAPI.Models.Cliente { id = 3, codigo = "33", nome = "Cliente 3" };
            //context.Cliente.Add(cliente3);

            //PedidosAPI.Models.Cliente cliente4 = new PedidosAPI.Models.Cliente { id = 4, codigo = "44", nome = "Cliente 4" };
            //context.Cliente.Add(cliente4);

            ////Adicionar produtos
            //PedidosAPI.Models.Produto produto1 = new PedidosAPI.Models.Produto { id = 1, codigo = "111", nome = "Produto 1", precoUnitario = 50 };
            //context.Produto.Add(produto1);

            //PedidosAPI.Models.Produto produto2 = new PedidosAPI.Models.Produto { id = 2, codigo = "222", nome = "Produto 2", precoUnitario = 55 };
            //context.Produto.Add(produto2);

            //PedidosAPI.Models.Produto produto3 = new PedidosAPI.Models.Produto { id = 3, codigo = "333", nome = "Produto 3", precoUnitario = 65 };
            //context.Produto.Add(produto3);

            //PedidosAPI.Models.Produto produto4 = new PedidosAPI.Models.Produto { id = 4, codigo = "444", nome = "Produto 4", precoUnitario = 70 };
            //context.Produto.Add(produto4);

            ////Adicionar pedido
            //PedidosAPI.Models.Pedido Pedido1 = new PedidosAPI.Models.Pedido();

            //Pedido1.Id = 1;
            //Pedido1.cliente = cliente1;
            //Pedido1.itensPedido.Add(new PedidosAPI.Models.ItemPedido { id = 1, produto = produto1, quantidade = 1 });
            //Pedido1.itensPedido.Add(new PedidosAPI.Models.ItemPedido { id = 2, produto = produto2, quantidade = 2 });

            //context.Pedido.Add(Pedido1);

            //context.SaveChanges();

        }
      
    }
}
