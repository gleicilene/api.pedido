﻿using Microsoft.EntityFrameworkCore;
using PedidosAPI.EntityConfig;
using PedidosAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PedidosAPI.Data
{
    public class DataContext: DbContext
    {
        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<Produto> Produto { get; set; }
        public DbSet<Pedido> Pedido { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        { 
            
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.ApplyConfiguration(new ClienteConfiguration());
            //   modelBuilder.Entity<Cliente>().HasOne(x => x.pedido).WithOne(x => x.cliente).HasForeignKey<Pedido>(b => b.idCliente);
        }


    }
}
