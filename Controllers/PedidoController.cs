﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using PedidosAPI.Data;
using PedidosAPI.Models;
using PedidosAPI.Servico;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PedidosAPI.Controllers
{
    [ApiController]
    [Route("v1/pedidos")]
    public class PedidoController : ControllerBase
    {
        [HttpGet]
        [Route("ConsultarPedidos")]
        public async Task<ActionResult<List<Pedido>>> Get([FromServices] DataContext context)
        {
            //var pedido = await context.Pedido                
            //                            .Include(s => s.cliente)
            //                            .Include(p=>p.itensPedido)
            //                            .ToListAsync();


            var pedido = await context.Pedido.Include(s => s.cliente as Cliente).ToListAsync();

            //var resp = pedido.Select(p => new
            // {
            //     id = p.Id ,
            //     cliente = p.cliente , 
            //     itensPedido = p.itensPedido.Select( i=> new
            //     {
            //         quantidade = i.quantidade ,
            //         produto = i.produto
            //     })

            // });


            return pedido;
        }


        [HttpPost]
        [Route("GerarPedido")]
        public async Task<ActionResult<Pedido>> Post(
            [FromServices] DataContext context,
            [FromBody]Pedido model)
        {

            //Validar se existe o cliente informado
            var p = context.Cliente.Where(c => c.id == model.ClienteId).FirstOrDefault();
            if (p == null)
            {
                //cliente inválido 
                return NotFound($"O codigo do cliente informado ( {model.ClienteId} ) não está cadastrado.");
            }

            //Validar se existe o produto informado
            var codprodutosDoPedido = model.itensPedido.Select(x => x.produto.codigo).ToList();
            List<string> codProdutosNaoCadastrados = new List<string>();
            foreach (string codproduto in codprodutosDoPedido)
            {
                if (!context.Produto.Any(c => c.codigo == codproduto))
                {
                    codProdutosNaoCadastrados.Add(codproduto);
                }
            }
            if (codProdutosNaoCadastrados.Count > 0)
            {
                return NotFound($"Os codigos de produtos não estão cadastrados( {string.Join(",", codProdutosNaoCadastrados.ToArray()) } ");
            }


            if (ModelState.IsValid)
            {
                model.frete = Frete.Calculafrete(model.itensPedido.Sum(x => x.quantidade));

                context.Pedido.Add(model);

                await context.SaveChangesAsync();
                return model;
            }
            else
            {
                return BadRequest(ModelState);
            }
        }
    }


}
