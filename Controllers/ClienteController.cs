﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PedidosAPI.Data;
using PedidosAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PedidosAPI.Controllers
{
    [ApiController]
    [Route("v1/clientes")]
    public class ClienteController : ControllerBase
    {
        [HttpGet]
        [Route("ConsultarClientes")]
        public async Task<ActionResult<List<Cliente>>> Get([FromServices] DataContext context)
        {
            var cliente = await context.Cliente.ToListAsync();
            return cliente;
        }

        [HttpPost]
        [Route("AdicionarCliente")]
        public async Task<ActionResult<Cliente>> Post(
            [FromServices] DataContext context,
            [FromBody]Cliente model)
        {
            if (ModelState.IsValid)
            {
                context.Cliente.Add(model);
                await context.SaveChangesAsync();
                return model;
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

    }
}
