﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PedidosAPI.Servico;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace PedidosAPI.Controllers
{
    [ApiController]
    [Route("v1/frete")]
    public class FreteController : ControllerBase
    {
        [HttpGet]
        [Route("")]
        public async Task<ActionResult<decimal>> Get(
        //public async Task<decimal> Post(
            decimal quantidade)
        {
            return Frete.Calculafrete(quantidade);
            
        }

    }
}
