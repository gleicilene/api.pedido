﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PedidosAPI.Servico
{
    public class Frete
    {
        public static decimal Calculafrete(decimal quantidade)
        {
            //Busca aleatória para calcular o frete
            //Conforme requisito, buscar o valor do frete entre R$ 5,00 e 10,00.
            var multiplicador = new Random().Next(5, 10);

            return (decimal)multiplicador * quantidade;
        }
    }
}
