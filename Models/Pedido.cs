﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PedidosAPI.Models
{
    [Table("Pedido")]
    public class Pedido
    {
        [Key]
        public int Id { get; set; }

        [Column("cliente")]
        public int ClienteId { get; set; }

        [NotMapped]
        public virtual Cliente cliente { get; set; }
        
        //[Column("cliente")]
        //public int idCliente { get; set; }

        private List<ItemPedido> _itensPedido;

        [NotMapped]
        public virtual List<ItemPedido> itensPedido
        {
            get
            {
                if (_itensPedido == null)
                {
                    _itensPedido = new List<ItemPedido>();
                }
                return _itensPedido;

            }
            set { _itensPedido = value; }
        }

        public decimal frete { get; set; }

    }
}
