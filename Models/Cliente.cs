﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;

namespace PedidosAPI.Models
{
    [Table("Cliente")]
    public class Cliente
    {
        [Key]
        [Required]
        public int id { get; set; }

        //[Required(ErrorMessage = "Este campo é obrogatório")]
        [MinLength(1, ErrorMessage = "Esse campo deve conter entre 1 e 10 caracteres")]
        [MaxLength(10, ErrorMessage = "Esse campo deve conter entre 1 e 10 caracteres")]
        public string codigo { get; set; }

        //[Required(ErrorMessage = "Este campo é obrogatório")]
        [MinLength(1, ErrorMessage = "Esse campo deve conter entre 1 e 150 caracteres")]
        [MaxLength(150, ErrorMessage = "Esse campo deve conter entre 1 e 150 caracteres")]
        public string nome { get; set; }

        //[Column("Codigo")]
        //public int PedidoId { get; set; }
        //[JsonIgnore]
        //public Pedido pedido { get; set; }
    }
}
