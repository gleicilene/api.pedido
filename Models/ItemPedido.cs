﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PedidosAPI.Models
{
    [Table("ItemPedido")]
    public class ItemPedido
    {
        [Key]
        public int id { get; set; }
        [Column("idPedido")]
        public int PedidoId { get; set; }

        public Produto produto { get; set; }

        [Required(ErrorMessage = "Este campo é obrogatório")]
        [Range(1, int.MaxValue, ErrorMessage = "A quantidade deve ser maior que zero")]
        public decimal quantidade { get; set; }

    }
}
