﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace PedidosAPI.Models
{
    public class Produto
    {
        [Key]
        public int id { get; set; }

        //[Required(ErrorMessage = "Este campo é obrogatório")]
        [MinLength(1, ErrorMessage = "Esse campo deve conter entre 1 e 10 caracteres")]
        [MaxLength(10, ErrorMessage = "Esse campo deve conter entre 1 e 10 caracteres")]
        public string codigo { get; set; }

        //[Required(ErrorMessage = "Este campo é obrogatório")]
        [MinLength(1, ErrorMessage = "Esse campo deve conter entre 1 e 200 caracteres")]
        [MaxLength(200, ErrorMessage = "Esse campo deve conter entre 1 e 200 caracteres")]
        public string nome { get; set; }

        //[Required(ErrorMessage = "Este campo é obrogatório")]
        [Range(1, int.MaxValue, ErrorMessage = "O preço unitário deve ser maior que zero")]
        public decimal precoUnitario { get; set; }
        public string imagemUrl { get; set; }

    }
}
